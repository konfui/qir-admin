import React, { Component } from 'react'
import { Container, Message } from 'semantic-ui-react'
import ChatBubble from './ChatBubble'

export default class ChatArea extends Component {

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom()
  }

  scrollToBottom() {
    this.el.scrollIntoView({ behavior: 'smooth' })
  }

  render() {
    let chats = []

    if (this.props.chatroom === undefined) {
      chats = <Message>Start by opening a chatroom!</Message>
    } else if (this.props.chatroom.chats !== undefined) {
      for (let id in this.props.chatroom.chats) {
        let chat = this.props.chatroom.chats[id]
        chats.push(<ChatBubble admin={chat.data['from_admin']} text={chat.data['content']} time={chat.data['timestamp']} key={id} />)
      }
    }
    if (chats.length === 0) {
      chats = <Message>You have no chats yet!</Message>
    }

    return (
      <Container fluid style={styles.container}>
        {chats}
        <div ref={el => { this.el = el; }} />
      </Container>

    )
  }
}

let styles = {
  container: {
    flexGrow: 1,
    padding: '16px',
    borderRadius: 0,
    overflowX: 'hidden',
    overflowY: 'overlay'
  }
}
