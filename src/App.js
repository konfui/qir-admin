import React, { Component } from 'react'
import { Icon, Menu, Dimmer, Container, Label, Loader, Message, Sidebar, Segment } from 'semantic-ui-react'
import Chatroom from './Chatroom'
import './App.css'

const adminServerURL = 'wss://evening-basin-25698.herokuapp.com'
export default class App extends Component {
  constructor() {
    super()
    this.state = {
      connection: undefined,
      chatrooms: {},
      activeRoom: '',
      loading: true,
      roomName: '',
      totalCount: 0,
      editing: false,
      shown: true,
      mobile: false
    }
    this.handleRoomChange = this.handleRoomChange.bind(this)
    this.handleChatSubmit = this.handleChatSubmit.bind(this)
  }

  componentWillMount() {
    this.updateOrientation()
  }

  updateOrientation() {
    let width = window.innerWidth
    if (width < 805) {
      this.setState({ mobile: true, shown: false })
    } else {
      this.setState({ mobile: false, shown: true })
    }
  }

  componentDidMount() {
    this.initWebsocket()
  }

  initWebsocket() {
    return new Promise((resolve, reject) => {
      window.WebSocket = window.WebSocket || window.MozWebSocket
      if (!window.WebSocket) {
        console.log("Websocket not supported!")
        reject()
        return
      }
      // open connection
      var connection = new WebSocket(adminServerURL)
      connection.onopen = function () {
        console.log("QiR Admin Server connected!")
        setInterval(() => {connection.send(JSON.stringify({type: "ping"}))}, 30000)
        resolve()
        return
      }
      connection.onerror = function (error) {
        console.log("Cannot connect to QiR Admin Server!")
        reject()
        return
      }
      connection.onmessage = function (message) {
        this.setState({ loading: false })
        var json
        try {
          json = JSON.parse(message.data)
        } catch (e) {
          console.log('Invalid JSON: ', message.data)
          return
        }

        // parse message
        let chatrooms = Object.assign({}, this.state.chatrooms)
        if (json.type === 'history') {
          console.log("Chat data recieved!")
          console.log(json)
          chatrooms = json.data
        } else if (json.type === 'new-room') {
          chatrooms[json.id] = ({ id: json.id, data: json.data, chats:[] })
        } else if (json.type === 'rename-room') {
          chatrooms[json.id].data['name'] = json.name
        } else if (json.type === 'recieve-chat') {
          chatrooms[json.chatroom].chats[json.id] = { id: json.id, data: json.data }
        } else if (json.type === 'clear-badge') {
          for (let id in chatrooms[json.chatroom].chats) {
            chatrooms[json.chatroom].chats[id].data['read'] = true
          }
        }
        this.setState({ chatrooms: chatrooms })
      }.bind(this)

      // check connectivity
      setInterval(function () {
        if (this.state.connection.readyState !== 1) {
          console.log("Server disconnected!")
          this.setState({ connection: new WebSocket(adminServerURL)})
        }
      }.bind(this), 10000)

      this.setState({ connection: connection })
    })
  }

  handleRoomChange = (e, { name }) => {
    this.setState({ activeRoom: name })
    let roomName = this.state.chatrooms[name].data['name']
    roomName = roomName === undefined ? '' : roomName
    this.setState({ roomName: roomName })
    if (this.state.mobile) this.setState({ shown: false })
  }

  handleRoomFocus = (e) => {
    this.clearBadge(this.state.activeRoom)
    if (this.state.editing) this.submitRoomNameChange()
  }

  beginEdit = (e) => {
    this.setState({ editing: true })
  }

  changeRoomNameChange = (e, { value }) => {
    this.setState({ roomName: value })
  }

  submitRoomNameChange = () => {
    if (this.state.roomName !== undefined && this.state.roomName.trim() !== '') {
      if (this.state.connection !== undefined) this.state.connection.send(JSON.stringify({type: "rename-room", old: this.state.activeRoom.trim(), new: this.state.roomName.trim()}))
    }
  }

  handleChatSubmit = (e, chat) => {
    if (this.state.connection !== undefined) {
      this.state.connection.send(JSON.stringify({type: "send-chat", data: chat, chatroom: this.state.activeRoom}))
      this.setState({ editing: false })
    }
    this.chatref.scrollDown()
  }

  clearBadge = (name) => {
    if (this.state.activeRoom === '') return
    if (this.unreadCount(this.state.chatrooms[this.state.activeRoom].chats) > 0) {
      if (this.state.connection !== undefined) this.state.connection.send(JSON.stringify({type: "clear-badge", chatroom: name}))
    }
  }

  unreadCount = (chats) => {
    let count = 0
    for (let id in chats) {
      if (!chats[id].data['read']) {
        count += 1
      }
    }
    return count
  }

  updateTotalBadge = (total) => {
    this.setState({ totalCount: total })
  }

  toggleSidebar = () => {
    if (this.state.mobile) this.setState({ shown: !this.state.shown })
  }

  handleSidebarHide = () => this.setState({ visible: false })

  render() {
    const { activeRoom } = this.state

    let chatroomList = []
    if (this.state.chatrooms === undefined) {
      chatroomList = <Message>You have no chatrooms yet!</Message>
    } else {
      let totalCount = 0
      for (let id in this.state.chatrooms) {
        let count = this.unreadCount(this.state.chatrooms[id].chats)
        totalCount += count
        let badge = count === 0 ? null : <Label circular size='mini' color='red'>{count}</Label>
        let roomName = this.state.chatrooms[id].data['name']
        roomName = roomName === undefined ? "Unnamed Room" : roomName
        chatroomList.push(
          <Menu.Item name={id} active={activeRoom === id} onClick={this.handleRoomChange} key={id} style={styles.roomItem}>
            <div style={{ overflow: 'hidden' }}>
              <div style={styles.roomText}>{roomName}</div>
              <div style={styles.roomId}>{id}</div>
            </div>
            <div style={styles.badge}>{badge}</div>
          </Menu.Item>
        )
        // this.updateTotalBadge(totalCount)
      }
      let showStyle
      let burgerButton
      if (this.state.mobile) {
        showStyle = this.state.shown ? styles.shownSidebar : styles.hiddenSidebar
        burgerButton = (
          <Menu.Item a='a' header onClick={this.toggleSidebar}>
            <Icon name='bars' />
            {totalCount !== 0 ? <Label circular size='mini' color='red' floating>{totalCount}</Label> : undefined}
          </Menu.Item>
        )
      }
      return (
        <div style={styles.app}>
          <Dimmer active={this.state.loading}>
            <Loader inverted content='Loading' />
          </Dimmer>
          <Menu inverted style={styles.navbar}>
            <Container>
              {burgerButton}
              <Menu.Item as='a' header>QiR Admin Console</Menu.Item>
            </Container>
          </Menu>
          <div style={styles.content}>
            <Sidebar.Pushable as={Segment} style={{ ...styles.sidebar, ...showStyle }}>
              <Sidebar
                as={Menu}
                onHide={this.handleSidebarHide}
                visible={this.state.shown}
                fluid
                vertical
                tabular
                style={{ ...styles.sidebarMenu, ...showStyle }}>
                {chatroomList}
              </Sidebar>
            </Sidebar.Pushable>
            <Chatroom
              ref={(ref) => { this.chatref = ref }}
              chatroom={this.state.chatrooms[activeRoom]}
              roomName={this.state.roomName}
              onFocus={this.handleRoomFocus}
              editing={this.state.editing}
              beginEdit={this.beginEdit}
              changeRoomNameChange={this.changeRoomNameChange}
              submitRoomNameChange={this.submitRoomNameChange}
              onChange={this.handleChatChange}
              onSubmit={this.handleChatSubmit} />
          </div>
        </div>
      )
    }
  }
}

let styles = {
  app: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    margin: 0
  },
  content: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    overflow: 'hidden'
  },
  navbar: {
    flexGrow: 0,
    margin: 0,
    borderRadius: 0
  },
  sidebar: {
    direction: 'rtl',
    paddingLeft: '1em',
    flexGrow: 0,
    flexBasis: '35%',
    overflowX: 'hidden',
    overflowY: 'overlay'
  },
  sidebarMenu: {
    minHeight: '100%',
    overflowX: 'hidden',
    overflowY: 'overlay'
  },
  roomItem: {
    direction: 'ltr',
    display: 'flex',
    flexDirection: 'row'
  },
  roomText: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    marginBottom: 5,
    fontSize: '1.1em'
  },
  roomId: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    flexGrow: 1,
    whiteSpace: 'nowrap',
    fontStyle: 'italic',
    color: '#999'
  },
  badge: {
    flexBasis: '10%',
    marginLeft: 'auto'
  },
  shownSidebar: {
    width: '100%',
    flexBasis: '100%'
  },
  hiddenSidebar: {
    width: 0,
    flexBasis: '0%'
  }
}
