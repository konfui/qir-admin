import React, { Component } from 'react'
import { Form, Input } from 'semantic-ui-react'

export default class ChatInput extends Component {
  state = {
    currentChat: ''
  }

  handleChange = (e, { value }) => this.setState({ currentChat: value })

  handleSubmit = (e) => {
    if (this.state.currentChat.trim() !== '') {
      this.props.onSubmit(e, this.state.currentChat)
      this.setState({ currentChat: '' })
    }
  }

  render() {

    return (
      <Form onSubmit={this.handleSubmit} style={styles.container}>
        <Input
          size='large' 
          icon='send' 
          placeholder='Write a message...' 
          value={this.state.currentChat} 
          onChange={this.handleChange} 
          onFocus={this.props.onFocus}
          style={styles.input}/>
      </Form>
    )
  }
}

let styles = {
  container: {
    display: 'flex',
    borderRadius: 0,
    padding: 14,
    paddingBottom: 20,
    paddingTop: 0,
    width: '100%',
    flexShrink: 0

  },
  input: {
    flexGrow: 1,
    borderRadius: 0,
    width: '100%'
  }
}
